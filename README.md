# API structure

I think healthcheck should just indicate that server has started so I wouldn't do much more in terms of exposing information over simple `/-/health`. Hostname (pod name in fact) should be just enough to start troubleshooting with observability stack.

Anyway when app gets dependencies (for e.g. redis) I'd add another endpoint to configure readiness probe.

# Server config

In such simple scenario it's hard to indicate what's best. Obviously for complicated python application I'd not probably run pure python and I would think about combining more complex servers like `uwsgi` and `bjork` and maybe even some reverse proxy with `nginx` or `haproxy` if some special magic would be required. Although I tend to keep it as simple as possible so just wanted to mention that's isn't normally end-scenario but in that case it would be ;).

# Continous Deployment

Continous deployment would be managed in GitOps (Argo CD) methdology by making a commit to `deploy/app//Chart.yaml` file (changin `appVersion` key). In more advanced use case I would modify `application.yaml` to create `ApplicationSet` with iterator for different namespaces / clusters (depending what environment isolation we implement) and probably add to `.gitlab-ci.yml` job responsible for making auto commits into either this repository (Chart + app versioned together approach) or to centralized repository responsible for holding all manifests (then probably both `application.yaml` and `chart` directory would go there as well)

# Chart config

To create helm chart - I just run `helm init` and configured values properly because I don't see a point in reinventing the wheel. If apps are maintained in 1 technology and require similiar feature set I opt for creating centralized helm chart and then configure it by substituing correct values.

Charts can sometimes become hard in maintenance so they are best option if there is somebody responsible for maintaing them. If there are more users advanced in kubernetes usage then kustomize may become easier solution if there is no need for packaging.

I added cpu based HPA but like to use prometheus operator to run scaling based on prometheus metrics. Beside this I have added podSecurityContext to not run docker as root.

# Tool stack

* Argo CD - in my opinion best for GitOps. Rich documentation a lot of useful features
* External DNS - out of the box DNSes pointing to our services / ingresses
* NGINX Ingress - probably the most popular right now (except cloud native ingresses).
* Cert Manager - most of DNS challenges API providers are similar set to those supported by External DNS so really often 1 API provider can help dealing with both configs.
* If registry for cluster is different than one native to cloud where cluster is running I opt for additional installation of ClusterSecret to solve once & for all issue with imagePullSecrets docker configs for each namespace.

# Infrastructure as code

I think it's hard to provide good code for Infrastructure as code without actually creating it so here are some top notes from what I would do:

* definetly would spawn a cluster using official [upstream module in terraform](https://registry.terraform.io/modules/terraform-google-modules/kubernetes-engine/google/latest)
* if possible (by budget) attach terraform code to app.terraform.io workspace to have continous configuration automation system, if no budget - then run on my own pipelines (gitlab I think has already support for keeping state in the repos so probably easy to implement) or atlantis server to eliminate running tf from local computers.
* Cert Manager / Nginx / External dns - all of that depending on the use case would define as argo cd applications or would use terraform and `helm_release` with `kubernetes_manifest` resources (sometimes scenarios for providing secrets make one or other scenario easier in maintenance)
