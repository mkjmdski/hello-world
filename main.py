import os
import socket
from flask import Flask, request, jsonify, make_response
from http import HTTPStatus

app = Flask(__name__)  # Standard Flask app


@app.route("/-/health", methods=['GET'])
def health():
    return make_response(jsonify({
        "hostname": socket.gethostname(),
        "status": "ok",
    }), HTTPStatus.OK)

@app.route("/api/echo", methods=['GET'])
def echo():
    text = request.args.get("text", None)
    if text:
        return make_response(jsonify({"text": text}), HTTPStatus.OK)
    return make_response(jsonify({"text": f"You did not provide 'text' param as an API argument"}), HTTPStatus.BAD_REQUEST)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=os.environ.get('listenport', 8080))
