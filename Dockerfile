FROM python:3.8-slim

WORKDIR /usr/src/app

ADD requirements.txt /usr/src/app/

RUN pip install --no-cache-dir -r requirements.txt

ADD main.py /usr/src/app/

EXPOSE 8080/tcp

CMD python main.py
